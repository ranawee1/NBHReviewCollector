from distutils.core import setup

setup(
    name='NBHReviewCollector',
    version='0.1dev',
    description='A workflow to analyze text data',
    autor='Meng Cai',
    author_email='caimeng2@msu.edu',
    packages=['NBHReviewCollector',],
    license='MIT',
    #long_description=open('README.md').read(),
    install_requires=[
        'numpy',
        'spacy',
        'pandas',
        'matplotlib',
        'nltk',
        'scikit-learn',
        'gensim',
        'TwitterAPI',
        'wordcloud',
        'pyLDAvis',
        'textblob'
    ])
