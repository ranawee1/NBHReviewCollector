# <center> Analyzing Text Data -- NBH ReviewCollector </center>

<img src="https://i.imgur.com/Qrwsyf1.png" width="60%">
<p style="text-align: right;">Image from: https://i.imgur.com/Qrwsyf1.png</p>

# Program Description

The large quantity of textual data provides new opportunities for urban researchers to examine people’s perceptions, attitudes, and behaviors. NBHReviewCollector enables automated social media data collection and text analysis (including both social media data and survey data). It is a toolbox that combines together existing python libraries for urban researchers to analyze text data.

Currently, there are four components in NBHReviewCollector: downloading tweets, plotting top words and phrases, finding salient topics, and detecting sentiments in text. 

A video presentation: https://mediaspace.msu.edu/media/NBHReviewCollector/1_re92h3oc

# Installation instructions

Set up a conda environment by running the following command:

    conda env create --prefix ./envs --file environment.yml

    conda activate ./envs
    
Clone the git repo:

    git clone https://gitlab.msu.edu/caimeng2/NBHReviewCollector.git

# Example usage

Run `example.ipynb` to see example usage.
